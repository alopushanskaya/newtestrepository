Feature: Admin moves through filtered results and saves details to csv files
As a SalesOptimizer Admin
I want to be able to move through filtered results
So that details are saved to csv files (one file for each company)

Scenario: SalesOptimizer Admin scenario
Given SalesOptimizer Admin is logged in
When Filters are applied
Then Search results are displayed
When Number of Companies found is saved
Then script runs 
