Given (/^SalesOptimizer Admin is logged in$/) do
    visit '/account/login'
    @login_page.username.set @login
    @login_page.password.set @password
    @login_page.submit.click
end
