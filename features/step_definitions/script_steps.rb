When (/^Number of Companies found is saved$/) do
  resultsCompaniesFoundNumber = find(:xpath, "//strong[@class='orange-so']")
  contents = resultsCompaniesFoundNumber.native.text
  p '--The number of Companies found is: ' + contents
end


Then (/^script runs$/) do

  begin
  @pageNumber = 1

    while  @general_page.has_companySitesInfoButtons? do

        @general_page.all(:xpath, "//a[contains(text(),'Info')]").each do |el|
          p " --- The el which i click : " + el.inspect
         new_window = window_opened_by {  el.click }
         within_window new_window do
          @site_page.wait_for_presenceOfCompanyInfo
          sleep 3
          @site_page.contactsNumber.select(50)
          @site_page.mustHaveEmailAddress.click
          sleep 3


          begin
            @rowNumber = 0
            @site_page.all(:xpath, "//*[@id='contactsTable']/tbody/tr").each do |row|
                @rowNumber += 1
                p " --- The row number : " + @rowNumber.to_s
                contactName = @site_page.setPageNumber(@rowNumber, 3).native.text
                p " --- The contact name : " + contactName
                contactPosition = @site_page.setPageNumber(@rowNumber, 4).native.text
                contactLevel = @site_page.setPageNumber(@rowNumber, 5).native.text
                contactDepartment = @site_page.setPageNumber(@rowNumber, 6).native.text
                contactEmailC = @site_page.setPageNumber(@rowNumber, 7).native.text
                contactEmail = contactEmailC.gsub(/\bC\b/, ' ')
                companyUrlxpath = find(:xpath, "//*[@id='root_url']/a")
                companyUrl = companyUrlxpath.native.text
                p '--The Company site name is: ' + companyUrl
                webSiteConfirmed = 'y'
                revenuexpath = find(:xpath, "//*[@id='estonlinevol']")
                revenue = revenuexpath.native.text
                yearsxpath = find(:xpath, "//*[@id='age']")
                years = yearsxpath.native.text
                facebookUrlxpath = find(:xpath, "//*[@id='fb']/a")
                facebookUrl = facebookUrlxpath.native.text
                facebookNumberOfLikesxpath = find(:xpath, "//*[@id='fb']/a")
                facebookNumberOfLikes = facebookNumberOfLikesxpath.native.text
                #facebookIfBot
                #productDescription
                contactCountry =  @site_page.setPageNumber(@rowNumber, 9).native.text


                #update files
                case @rowNumber
                  when 1
                    CSV.open("file1LeadList#{@number}.csv", "a+") do |csv|
                      csv << [contactName, contactPosition, contactEmail, companyUrl, webSiteConfirmed, revenue, years, facebookUrl, facebookNumberOfLikes, contactCountry]
                    end
                  when 2
                    CSV.open("file2LeadList#{@number}.csv", "a+") do |csv|
                      csv << [contactName, contactPosition, contactEmail, companyUrl, webSiteConfirmed]
                    end
                  when 3
                    CSV.open("file3LeadList#{@number}.csv", "a+") do |csv|
                      csv << [contactName, contactPosition, contactEmail, companyUrl, webSiteConfirmed]
                    end
                  else
                end
              end

                  @site_page.execute_script "window.close()"
                  p " --- The site page is CLOSED --- "


              #   begin
              #     @site_page.clickNextButton.click
              #     p " --- The another page button is clicked the site page in BEGIN section "
              #   rescue RestClient::ExceptionWithResponse => e
              #    @site_page.execute_script "window.close()"
              #     p " --- The site page is CLOSED in BEGIN section  "
              #     break
              #   end
              # end


              # nextPageClickable = expect{ @site_page.nextRowsPage(@rowsPageIndex).click }.not_to raise_error #(Selenium::WebDriver::Error::UnknownError)
              #   if nextPageClickable
              #     @site_page.nextRowsPage(@rowsPageIndex).click
              #     p " --- The another page button is clicked the site page in IF section "
              #     @rowsPageIndex += 1
              #     redo
              #   else
              #     @site_page.execute_script "window.close()"
              #     p " --- The site page is CLOSED in IF section  "
              #     break
              #   end


                # if @site_page.nextRowsPage(@rowsPageIndex).displayed?
                #   #@site_page.nextRowsPage(@rowsPageIndex).enabled?
                #   @site_page.nextRowsPage(@rowsPageIndex).click
                #   p " --- The another page button is clicked the site page in IF section "
                #   sleep 10
                #   @rowsPageIndex += 1
                # else
                #   @site_page.execute_script "window.close()"
                #   p " --- The TAB is CLOSED in IF section  "
                #   break
                # end
                #


                # if @general_page.clickNextCompaniesPage do
                #   p " --- The nextCompanyPageButton button is clicked the General page in IF section "
                #     @pageNumber += 1
                #   end
                # else
                #   break

                end
            end
      end
    end




  begin
    @general_page.clickNextCompaniesPage(pageNumber)
  rescue RestClient::ExceptionWithResponse => e
                break
              end


  redo

end
end
