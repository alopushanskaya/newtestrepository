require 'cucumber'
require 'rspec'
require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'selenium-webdriver'
#require_relative 'feature_helper'
require 'securerandom'
require 'require_all'
require 'rest-client'
require_rel '../../pages'
require "csv"


Capybara.app_host = 'https://app.salesoptimize.com/'

#setting Capybara driver
Capybara.default_driver = :selenium
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[window-size=1024,768]))
end

  Before do
    @general_page = GeneralPage.new
    @login_page = LoginPage.new
    @site_page = SitePage.new
  end

After do
  #setting Capybara driver to reset sessions after all tests are done
  Capybara.reset_sessions!
end
