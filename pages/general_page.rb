class GeneralPage < SitePrism::Page
  element :search_results_counts, "div[class='form-group summary-status']"
  elements :companySitesInfoButtons , :xpath, "//a[contains(text(),'Info')]"
  element :presenceOfFacebook, :xpath, "//*[@id='facebooklab']"

  def clickNextCompaniesPage (pageNumber)
    find(:xpath, "//*[@id='mainform']/div/div[3]/div[3]/ul/li[#{pageNumber}]/a" ).click
  end
end
