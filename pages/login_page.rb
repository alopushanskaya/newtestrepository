class LoginPage < SitePrism::Page
  element :username, "input[id='signupEmail']"
  element :password, "input[id='signupPassword']"
  element :submit, "button[id='loginAction']"
end
