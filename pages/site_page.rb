class SitePage < SitePrism::Page
element :presenceOfCompanyInfo, :xpath, "//*[@id='companyinfo']"
element :mustHaveEmailAddress, :xpath,  "//*[@id='emailcontainer']/div/label"
#element :clickNextButton, :xpath,  "//*[@id='contactsTable_next']/av"
elements :rowsOfContacts , :xpath, "//*[@id='contactsTable']/tbody/tr"
element :contactsNumber, :xpath, "//*[@id='contactsTable_length']/label/select"

def setPageNumber (rowNumber, columnNumber)
  find(:xpath, "//*[@id='contactsTable']/tbody/tr[#{rowNumber}]/td[#{columnNumber}]")
end

def nextRowsPage (rowsPageIndex)
  find(:xpath, "//*[@id='contactsTable_paginate']/ul/li[#{rowsPageIndex}]/a" )
end

end
